# Prueba de Ionix

## Instalar minikube

[Guide](https://minikube.sigs.k8s.io/docs/start/)

## Instalar helm
[Guide](https://helm.sh/docs/intro/install/)

## Build la api .jar
```
mvn clean install
```

## Publicar la imagen en docker hub
```
docker build -t ionix-api:stable "./"
```

```
docker tag ionix-api:stable jalpino/ionix:
```

```
docker push
```

## Implementar imagen en el pod del kluster minikube
### Visualizar despliegue para ver errores 
```
helm install --dry-run debug .
```
### Listar despliegues por helm en el cluster
```
helm list
```
### Desinstalar despliegues por helm en el cluster
```
helm uninstall chart-
```
### Empaquetar chart
```
helm package .
```
### Desplegar chart en minikube

```
helm install . --generate-name
```

### Mostrar los logs del POD
```
kubectl logs pod/chart-1690486939-api-ionix
```

## Ejercicio 1:
### Mostrar los logs del contenedor
```
kubectl logs pod/chart-1690496790-api-ionix
```
### Entrar dentro del contenedor
```
kubectl exec -it pod/chart-1690496790-api-ionix sh
```
### Mostrar las variables de entorno dentro del contenedor
```
kubectl exec -it pod/chart-1690496790-api-ionix printenv 
```

### Actualizar la variable de entorno y desplegar de nuevo el chart
```
helm upgrade --set service.eviromentName="desde la consola"  chart-1690500893 ./api-ionix/
```

## Publicar el api a traves de forward
### Get the application URL by running these commands:

```
export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=api-ionix,app.kubernetes.io/instance=chart-1690496790" -o jsonpath="{.items[0].metadata.name}")
```

```
export CONTAINER_PORT=$(kubectl get pod --namespace default $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
```

```
echo "Visit http://127.0.0.1:8080 to use your application"
```
```
kubectl --namespace default port-forward $POD_NAME 8080:$
```

### Ejercicio 2:
En el archivo .gitlab-ci.yml se encuentra el proceso de CI/CD del API, donde realizan los siguientes pasos:

1. Se inicia haciendo build de la app en java

2. Luego se prueba los cambios en el chart para publicar en el cluster para revisar si hay algun error en el codigo del chart

3. Finalmente se despliega el chart en el cluster utilizando helm

### Ejercicio 3: 
#### A. Se pueden pasar las variables de entorno configuradas en el pipeline de gitlab de la siguiente manera al deploy del chart con helm suponiendo que tenemos dos variables de entorno en el stage de gitlab pipeline, y tenemos dos variables credenciales en el archivo values del chart

```
USERNAME=jalpino
PASSWORD=123456
```
```
helm install . --generate-name --set service.port=1111 --set credentials.username=$USERNAME --set credentials.password=$PASSWORD
```
### B. Nombres de objetos en cluster de kubernetes
    
```    
Pod
Container
Deployment
Secret
```
### C. La mejor manera de manejar las modificaciones de una base de datos, especificamente el schema, es migrations. Ya que validan las migraciones historicas (modificaciones), se puede hacer rolback, si es que se agrega el codigo en cada migration y las modificaciones son atomicas.
