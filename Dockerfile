#Version 1.0.1
FROM amazoncorretto:17-alpine-jdk
ENV TZ=America/Santiago
ENV NAME="Ionix"
VOLUME /tmp
EXPOSE 5050
ARG JAR_FILE=api/greetingAPI.jar
ENV JAVA_OPTS ""

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar /app.jar"]